from tasks.views import CreateTaskFormView, TaskListView
from django.urls import path

urlpatterns = [
    path("create/", CreateTaskFormView, name="create_task"),
    path("mine/", TaskListView, name="show_my_tasks"),
]
