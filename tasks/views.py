from django.shortcuts import render, redirect
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from tasks.forms import CreateTaskForm


@login_required
def CreateTaskFormView(request):
    if request.method == "POST":
        task_form = CreateTaskForm(request.POST)
        if task_form.is_valid():
            task_form = task_form.save()
            return redirect("list_projects")
    else:
        task_form = CreateTaskForm()
    context = {"task_form": task_form}
    return render(request, "tasks/create_task.html", context)


@login_required
def TaskListView(request):
    task_list = Task.objects.filter(assignee=request.user)

    context = {"task_list": task_list}
    return render(request, "tasks/show_my_tasks.html", context)
