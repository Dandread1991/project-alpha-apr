from projects.views import (
    ProjectListView,
    ProjectDetailView,
    CreateProjectFormView,
)
from django.urls import path

urlpatterns = [
    path("", ProjectListView, name="list_projects"),
    path("<int:id>/", ProjectDetailView, name="show_project"),
    path("create/", CreateProjectFormView, name="create_project"),
]
