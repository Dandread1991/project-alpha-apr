from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import CreateProjectForm


# Create your views here.
@login_required
def ProjectListView(request):
    project_list = Project.objects.filter(owner=request.user)

    context = {"project_list": project_list}
    return render(request, "projects/projects.html", context)


@login_required
def ProjectDetailView(request, id):
    project_instance = get_object_or_404(Project, id=id)
    context = {"project_instance": project_instance}
    return render(request, "projects/project_detail.html", context)


@login_required
def CreateProjectFormView(request):
    if request.method == "POST":
        project_form = CreateProjectForm(request.POST)
        if project_form.is_valid():
            project_form = project_form.save()
            return redirect("list_projects")
    else:
        project_form = CreateProjectForm()
    context = {"project_form": project_form}
    return render(request, "projects/create_project.html", context)
