from accounts.views import LoginFormView, LogOutView, SignUpFormView
from django.urls import path

urlpatterns = [
    path("login/", LoginFormView, name="login"),
    path("logout/", LogOutView, name="logout"),
    path("signup/", SignUpFormView, name="signup"),
]
